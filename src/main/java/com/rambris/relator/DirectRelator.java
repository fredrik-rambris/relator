/**
 * DirectRelator.java version $id$
 * <p>
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

import com.rambris.relator.configuration.RelatorConfiguration;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Relations are created as they are added.
 *
 * @author Fredrik Rambris <fredrik@rambris.com>
 */
@Slf4j
public class DirectRelator extends Relator {
    protected ConcurrentHashMap<Object, ConcurrentHashMap<Object, Relation>> relations;

    public DirectRelator(Loader loader, RelatorConfiguration config) {
        super(loader, config);
        relations = new ConcurrentHashMap<>();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.rambris.relator.Relator#addRelation(Object, Object)
     */
    @Override
    public void addRelation(Object actionobj, Object relatedobj) {
        try {
            Object action = toAction(actionobj);
            Object related = toRelated(relatedobj);

            ConcurrentHashMap<Object, Relation> rs = relations.get(action);
            if (rs == null) {
                rs = new ConcurrentHashMap<>();
                relations.put(action, rs);
            }
            Relation r = rs.get(related);
            if (r == null) {
                r = new Relation(related);
                rs.put(related, r);
            }
            r.count++;
        } catch (NumberFormatException e) {
            return;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.rambris.relator.Relator#clearRelations()
     */
    @Override
    public void clearRelations() {
        relations.clear();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.rambris.relator.Relator#getRelations(Object)
     */
    @Override
    public List<Relation> getRelations(Object actionobj) {
        try {
            Object action = toAction(actionobj);

            ConcurrentHashMap<Object, Relation> rs = relations.get(action);
            if (rs == null) return null;
            List<Relation> ret = new LinkedList<>(rs.values());
            Collections.sort(ret);
            return ret;
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
