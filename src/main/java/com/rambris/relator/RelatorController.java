package com.rambris.relator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "", produces = "application/json")
public class RelatorController {

    @Autowired
    private RelatorApplication app;

    @GetMapping("/{relatorName}/{actionId}")
    public Object[] getRelations(@PathVariable String relatorName, @PathVariable String actionId) {
        return app.getRelations(relatorName, actionId);
    }
}
