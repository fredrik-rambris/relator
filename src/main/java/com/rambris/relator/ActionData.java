/**
 * ActionData.java version $id$
 * <p>
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class handling storage and matching of the actions metadata. This could be a
 * products data such as title, thumbnail and stock
 *
 * @author Fredrik Rambris <fredrik@rambris.com>
 *
 */
public class ActionData {
    /** The data keyd with actionId */
    private final ConcurrentHashMap<Object, Map<String, Object>> data;

    private final Loader loader;

    public ActionData(Loader loader) {
        this.loader = loader;
        data = new ConcurrentHashMap<>();
    }

    public Map<String, Object> getActionData(Object actionId) {
        return data.get(actionId);
    }

    public void loadActionData() {
        this.loadActionData(null);
    }

    public void loadActionData(String source) {
        loader.loadAcionData(this, source);
    }

    public void addActionDatum(Object actionId, String key, Object value) {
        Map<String, Object> row = data.get(actionId);
        if (row == null) {
            row = new HashMap<>();
            data.put(actionId, row);
        }
        row.put(key, value);
    }

    public void addActionData(Object actionId, Map<String, Object> row) {
        data.put(actionId, row);
    }
}
