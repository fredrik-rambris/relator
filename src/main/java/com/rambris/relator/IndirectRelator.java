/**
 * IndirectRelator.java version $id$
 * <p>
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

import com.rambris.relator.configuration.RelatorConfiguration;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Indirect relation is action => actors => actions. I.e. if action is product
 * and actor is persons it could represent other persons that bought this
 * product also bought these products
 *
 * @author Fredrik Rambris <fredrik@rambris.com>
 *
 */
@Slf4j
public class IndirectRelator extends Relator {
    /* actor=>actions */
    private final ConcurrentHashMap<Object, LinkedList<Object>> actors;
    /* action=>actors */
    private final ConcurrentHashMap<Object, LinkedList<Object>> actions;

    public IndirectRelator(Loader loader, RelatorConfiguration config) {
        super(loader, config);
        actors = new ConcurrentHashMap<>();
        actions = new ConcurrentHashMap<>();
    }

    /**
     * Add relation between actor and action. This could be a person and a
     * bought product. This will create two relations. actor=>action and
     * action=>actor
     *
     * @param actor
     *            The actor, person, artist etc.
     * @param action
     *            The action, product, instrument etc.
     */
    @Override
    public void addRelation(Object action, Object actor) {
        try {
			/*
			Object action = toAction(actionobj);
			Object actor = toRelated(actorobj);
			*/

            LinkedList<Object> p;
            if ((p = actors.get(actor)) == null) {
                p = new LinkedList<Object>();
                actors.put(actor, p);
            }
            p.add(action);

            if ((p = actions.get(action)) == null) {
                p = new LinkedList<Object>();
                actions.put(action, p);
            }
            p.add(actor);
        } catch (NumberFormatException e) {
        }
    }

    /**
     * Get the calculated indirect relations for the action.
     *
     * @param actionobj
     *            The action, product etc.
     * @return A vector of related actions based on what other actors did. Say
     *         other persons that bought this product also bought...
     */
    @Override
    public List<Relation> getRelations(Object actionobj) {
        try {
            Object actionId = toAction(actionobj);
            if (actions.get(actionId) == null) return null;
            Map<Object, Relation> relations = new HashMap<>();
            for (Object actor_id : actions.get(actionId)) {
                for (Object related_action : actors.get(actor_id)) {
                    if (related_action.equals(actionId)) continue;
                    Relation r = relations.get(related_action);
                    if (r == null) {
                        r = new Relation(related_action);
                        relations.put(related_action, r);
                    }
                    r.count++;
                }
            }
            List<Relation> ret = new LinkedList<>(relations.values());
            Collections.sort(ret);
            return ret;
        } catch (NumberFormatException e) {
            return new LinkedList<>();
        }
    }

    @Override
    public void clearRelations() {
        actors.clear();
        actions.clear();
    }
}
