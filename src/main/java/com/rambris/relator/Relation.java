/**
 * Relation.java version $id$
 * 
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

/**
 * @author Fredrik Rambris <fredrik@rambris.com>
 * 
 */
public class Relation implements Comparable<Relation>
{
	Object actionId;
	Integer count;

	public Relation(Object actionId)
	{
		this.actionId = actionId;
		this.count = 0;
	}

	@Override
	public int compareTo(Relation relation)
	{
		/*
		 * We do a backwards comparison to get the list sorted in descending
		 * order
		 */
		return relation.count.compareTo(count);
	}

	@Override
	public String toString()
	{
		return actionId + ":" + count;
	}
}
