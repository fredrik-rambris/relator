/**
 * Relator.java version $id$
 * <p>
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

import com.rambris.relator.configuration.RelatorConfiguration;

import java.util.List;

/**
 * Relator stores relations between actions etc. and returns the top relations
 * based on later input.
 *
 * @author Fredrik Rambris <fredrik@rambris.com>
 */
public abstract class Relator {
    protected RelatorConfiguration config;
    protected Loader loader;

    public Relator(Loader loader, RelatorConfiguration config) {
        this.loader = loader;
        this.config = config;
    }

    /**
     * Add relation between action and relation.
     *
     * @param action
     * @param related
     */
    public abstract void addRelation(Object action, Object related);

    /**
     * Removed all relations from memory
     */
    public abstract void clearRelations();

    /**
     * Get the relations for the supplied action
     *
     * @param action
     * @return list of relations with the one with the most connections first.
     */
    public abstract List<Relation> getRelations(Object action);

    public void loadRelations() {
        loadRelations(null);
    }

    /**
     * Load a bunch of relations from the resident Loader
     *
     * @param source
     *            an optional source for the loader
     */
    public void loadRelations(String source) {

        loader.loadRelations(this, source);
    }


    protected Object toAction(Object action) {
        String type = config.getType();
        return toObject(type, action);
    }

    protected Object toRelated(Object related) {
        String type = config.getRelatedtype();
        return toObject(type, related);
    }

    private Object toObject(String type, Object obj) {
        if (type.equalsIgnoreCase("String")) {
            if (obj instanceof String) return obj;
            else return obj.toString();
        } else if (type.equalsIgnoreCase("Long")) {
            if (obj instanceof String) return Long.parseLong((String) obj);
            else return obj;

        } else if (type.equalsIgnoreCase("Integer") || type.equalsIgnoreCase("Int")) {
            if (obj instanceof String) return Integer.parseInt((String) obj);
            return obj;
        }
        return obj;
    }

}
