package com.rambris.relator.configuration;

import lombok.Data;

@Data
public class Column {
    private int index;
    private String destination;
    private String className;
}
