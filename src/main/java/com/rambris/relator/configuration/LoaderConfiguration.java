package com.rambris.relator.configuration;

import lombok.Data;

import java.util.List;

@Data
public class LoaderConfiguration {
    private String name;
    private String type;
    private String query;
    private String path;
    private String delimiter="TAB";
    private boolean flattened=true;
    private List<Column> columns;

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter!=null?delimiter.toUpperCase():"TAB";
    }
}
