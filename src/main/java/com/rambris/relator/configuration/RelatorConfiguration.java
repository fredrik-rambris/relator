package com.rambris.relator.configuration;

import lombok.Data;

@Data
public class RelatorConfiguration {
    private String name;
    private String type;
    private String loader;
    private String actiontype;
    private String relatedtype;
}
