package com.rambris.relator.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties
public class RelatorsConfiguration {
    private List<RelatorConfiguration> relators;
    private List<LoaderConfiguration> loaders;
}

