/**
 * Loader.java version $id$
 * <p>
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

import com.rambris.relator.configuration.Column;
import com.rambris.relator.configuration.LoaderConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Fredrik Rambris <fredrik@rambris.com>
 */
@Slf4j
public abstract class Loader {
    protected final LoaderConfiguration config;
    protected final ApplicationContext context;
    Map<String, Integer> sourceCols;
    Map<Integer, String> destinationCols;
    Map<String, Class> colTypes;

    /* If the actiondata is stored in separate columns
     * like so (flattened=true)
     * actionid|title|description|thumbnail|price|stock
     * or like this (flattened=false)
     * actionid|name       |value
     * 1       |title      |Box of chocolate
     * 1       |description|Exclusive chocolate
     * 1       |thumbnail  |/img/123/123-thumbnail.jpg
     * 1       |price      |49.95
     * 1       |stock      |42
     */
    protected boolean flattened = true;

    public Loader(LoaderConfiguration config, ApplicationContext context) {
        this.config = config;
        this.context = context;
        sourceCols = new HashMap<String, Integer>();
        destinationCols = new HashMap<Integer, String>();
        colTypes = new HashMap<String, Class>();

        for (Column column : config.getColumns()) {
            String destination = column.getDestination().toLowerCase();
            int index = column.getIndex();
            String className = column.getClassName();
            if (className != null) {
                if (className.equalsIgnoreCase("int") || className.equalsIgnoreCase("Integer"))
                    colTypes.put(destination, Integer.class);
                else if (className.equalsIgnoreCase("long")) colTypes.put(destination, Long.class);
                else if (className.equalsIgnoreCase("float")) colTypes.put(destination, Float.class);
                else if (className.equalsIgnoreCase("double")) colTypes.put(destination, Double.class);
                else if (className.equalsIgnoreCase("string")) colTypes.put(destination, String.class);
                else if (className.equalsIgnoreCase("bool") || className.equalsIgnoreCase("Boolean"))
                    colTypes.put(destination, Boolean.class);
                else {
                    log.info("Unknown column type " + className + " for destination " + destination + ". Defaulting to String");
                    colTypes.put(destination, String.class);
                }
            }

            sourceCols.put(destination, index);
            destinationCols.put(index, destination);
        }
        flattened = config.isFlattened();
    }

    @Override
    public String toString() {
        return config.getName();
    }

    /**
     * Cast a string to the given class
     *
     * @param string
     * @param toClass
     * @return
     */
    protected Object castString(String string, Class toClass) {
        if (string == null) return null;
        if (toClass == Integer.class) return new Integer(string);
        else if (toClass == Long.class) return new Long(string);
        else if (toClass == Float.class) return new Float(string);
        else if (toClass == Double.class) return new Double(string);
        else if (toClass == Boolean.class) {
            char begin = string.toLowerCase().charAt(0);
            if (begin == 'y' || begin == 'j' || begin == '1' || string.equalsIgnoreCase("true"))
                return new Boolean(true);
            else return new Boolean(false);
        }
        return string;
    }

    public abstract void loadRelations(Relator relator, String source);

    public abstract void loadAcionData(ActionData actionData, String source);
}
