/**
 * FileLoader.java version $id$
 * <p>
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

import com.rambris.relator.configuration.LoaderConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fredrik Rambris <fredrik@rambris.com>
 *
 */
@Slf4j
public class FileLoader extends Loader {
    private final Map<String, String> delimiters;

    /**
     * @param config
     */
    public FileLoader(LoaderConfiguration config, ApplicationContext context) {
        super(config, context);
        delimiters = new HashMap<>();
        delimiters.put("TAB", "\t");
        delimiters.put("SPACE", "\\s+");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.rambris.relator.Loader#loadAcionData(com.rambris.relator.ActionData,
     *      java.lang.String)
     */
    @Override
    public void loadAcionData(ActionData actionData, String source) {

    }

    /*
     * (non-Javadoc)
     *
     * @see com.rambris.relator.Loader#loadRelations(com.rambris.relator.Relator,
     *      java.lang.String)
     */
    @Override
    public void loadRelations(Relator relator, String source) {
        String delimiter = config.getDelimiter();
        if (delimiters.containsKey(delimiter)) delimiter = delimiters.get(delimiter);

        if (source == null) {
            source = config.getPath();
        }
        if (source == null) return;
        try(BufferedReader in = new BufferedReader(new FileReader(source))) {
            int actionCol = sourceCols.get("action");
            int relatedCol = sourceCols.get("related");

            String row;
            while ((row = in.readLine()) != null) {
                String[] parts = row.split(delimiter);
                if (parts.length < 2) continue;
                Object action = parts[actionCol - 1];
                Object related = parts[relatedCol - 1];
                if (colTypes.get("action") != null) action = castString((String) action, colTypes.get("action"));
                if (colTypes.get("related") != null) related = castString((String) related, colTypes.get("related"));
                relator.addRelation(action, related);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

}
