/**
 * DatabaseLoader.java version $id$
 * <p>
 * Copyright (c) 2008 Fredrik Rambris <fredrik@rambris.com>. All rights reserved.
 */
package com.rambris.relator;

import com.rambris.relator.configuration.LoaderConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fredrik Rambris <fredrik@rambris.com>
 *
 */
@Slf4j
public class DatabaseLoader extends Loader {
    private DataSource dataSource;

    /**
     * @param config
     * @throws ClassNotFoundException
     */
    public DatabaseLoader(LoaderConfiguration config, ApplicationContext context) throws ClassNotFoundException {
        super(config, context);
        dataSource = context.getBean(DataSource.class);
    }

    private Object getRSObject(ResultSet rs, int columnIndex, int type) throws SQLException {
        switch (type) {
            case Types.BIGINT:
                return rs.getLong(columnIndex);
            case Types.BIT:
            case Types.BOOLEAN:
                return rs.getBoolean(columnIndex);
            case Types.CHAR:
            case Types.VARCHAR:
                return rs.getString(columnIndex);
            case Types.FLOAT:
                return rs.getFloat(columnIndex);
            case Types.REAL:
            case Types.DOUBLE:
            case Types.DECIMAL:
                return rs.getDouble(columnIndex);
            case Types.DATE:
                java.sql.Date d = rs.getDate(columnIndex);
                return new java.util.Date(d.getTime());
            case Types.TIME: {
                java.sql.Time t = rs.getTime(columnIndex);
                return new java.util.Date(t.getTime());
            }
            case Types.TIMESTAMP: {
                java.sql.Timestamp t = rs.getTimestamp(columnIndex);
                return new java.util.Date(t.getTime());
            }
            case Types.INTEGER:
            case Types.NUMERIC:
                return rs.getInt(columnIndex);
            case Types.SMALLINT:
                return rs.getShort(columnIndex);
            case Types.TINYINT:
                return rs.getShort(columnIndex);
        }
        return null;
    }

    private Map<String, Object> getRSMap(ResultSet rs) throws SQLException {
        Map<String, Object> map = new HashMap<>();
        ResultSetMetaData metadata = rs.getMetaData();
        for (int c = 0; c < metadata.getColumnCount(); c++) {
            map.put(metadata.getColumnLabel(c + 1), getRSObject(rs, c, metadata.getColumnType(c)));
        }
        return map;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.rambris.relator.Loader#loadAcionData(com.rambris.relator.ActionData,
     *      java.lang.String)
     */
    @Override
    public void loadAcionData(ActionData actionData, String source) {
        if (source == null) {
            source = config.getQuery();
        }
        int actionIdCol = 1;
        int keyCol = 2;
        int valCol = 3;
        if (sourceCols.get("actionid") != null) actionIdCol = sourceCols.get("actionid");

        if (!flattened) {
            if (sourceCols.get("key") != null) actionIdCol = sourceCols.get("key");
            if (sourceCols.get("val") != null) actionIdCol = sourceCols.get("val");
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(source);
            ResultSetMetaData metadata = rs.getMetaData();
            while (rs.next()) {
                Object actionId = getRSObject(rs, actionIdCol, metadata.getColumnType(actionIdCol));

                if (flattened) {
                    Map<String, Object> row;
                    if (sourceCols.size() == 0) {
                        row = getRSMap(rs);
                        actionData.addActionData(actionId, row);
                    } else {
                        row = new HashMap<>();
                        for (int c = 0; c < metadata.getColumnCount(); c++) {
                            String destination = destinationCols.get(c + 1);
                            if (destination != null) {
                                row.put(destination, getRSObject(rs, c, metadata.getColumnType(c)));
                            }
                        }
                        actionData.addActionData(actionId, row);
                    }
                } else {
                    String key = rs.getString(keyCol);
                    Object val = getRSObject(rs, valCol, metadata.getColumnType(valCol));
                    actionData.addActionDatum(actionId, key, val);
                }
            }
        } catch (SQLException e) {
            log.error(source, e);
        } finally {
            closeAll(conn, stmt, rs);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.rambris.relator.Loader#loadRelations(com.rambris.relator.Relator,
     *      java.lang.String)
     */
    @Override
    public void loadRelations(Relator relator, String source) {
        if (source == null) {
            source = config.getQuery();
        }
        log.debug("loadRelations from {}", source);
        int actionCol = sourceCols.get("action");
        int relatedCol = sourceCols.get("related");

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(source);
            ResultSetMetaData meta = rs.getMetaData();
            log.debug("Getting rows");
            while (rs.next()) {
                Object action = getRSObject(rs, actionCol, meta.getColumnType(actionCol));
                Object related = getRSObject(rs, relatedCol, meta.getColumnType(relatedCol));
                relator.addRelation(action, related);
                if(rs.getRow()%10000==0) log.info("Got row {}", rs.getRow());
            }
        } catch (SQLException e) {
            log.error(source, e);
        } finally {
            closeAll(conn, stmt, rs);
        }

    }

    private void closeAll(Connection conn, Statement stmt, ResultSet rs) {
        closeResultset(rs);
        closeStatement(stmt);
        closeConnection(conn);
    }

    private void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                log.warn(e.getMessage(), e);
            }
        }
    }

    private void closeResultset(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                log.warn(e.getMessage(), e);
            }
        }
    }

    private void closeStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                log.warn(e.getMessage(), e);
            }
        }
    }

    private Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
