package com.rambris.relator;

import com.rambris.relator.configuration.LoaderConfiguration;
import com.rambris.relator.configuration.RelatorConfiguration;
import com.rambris.relator.configuration.RelatorsConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootApplication
@Slf4j
@EnableConfigurationProperties
public class RelatorApplication implements CommandLineRunner {
    private Map<String, Loader> loaders;
    private Map<String, Relator> relators;

    @Autowired
    RelatorsConfiguration relatorsConfiguration;

    @Autowired
    ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(RelatorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        initLoaders();
        initRelators();
        log.info("Relators: {}", relators);
        for (Relator r : relators.values()) {
            log.debug("Loading relations for {}", r.config.getName());
            r.loadRelations();
        }
        System.gc();
        log.info("Application ready");
    }

    public Object[] getRelations(String relatorName, Object action) {
        Relator r = relators.get(relatorName);
        if (r == null) {
            log.warn("Relator {} not found", relatorName);
            return null;
        }
        return r.getRelations(action).stream()
                .filter(rel -> rel.count > 2)
                .limit(30)
                .collect(Collectors.toList()).toArray();
    }

    private void initLoaders() throws ClassNotFoundException {
        loaders = new HashMap<>();
        for (LoaderConfiguration loaderConfig : relatorsConfiguration.getLoaders()) {
            String loaderName = loaderConfig.getName();
            String loaderType = loaderConfig.getType();
            if (loaderName != null && loaderType != null) {
                Loader loader = newLoader(loaderConfig);
                loaders.put(loaderName, loader);
            }

        }
    }

    private Loader newLoader(LoaderConfiguration loaderConfig) throws ClassNotFoundException {
        String loaderType = loaderConfig.getType();
        Loader loader;
        Class loaderClass = Class.forName(this.getClass().getPackage().getName() + "." + loaderType);
        if (Loader.class.isAssignableFrom(loaderClass)) {
            try {
                Constructor<Loader> c = loaderClass.getConstructor(LoaderConfiguration.class, ApplicationContext.class);
                loader = c.newInstance(loaderConfig, context);
                return loader;
            } catch (InstantiationException | IllegalAccessException | SecurityException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException e) {
                throw new ClassNotFoundException("Cannot find class: " + loaderClass);
            }
        } else throw new ClassNotFoundException(loaderType + " is not a valid subclass of Loader");
    }

    private void initRelators() throws ClassNotFoundException {
        relators = new HashMap<>();
        for (RelatorConfiguration relatorConfig : relatorsConfiguration.getRelators()) {
            String relatorName = relatorConfig.getName();
            String relatorType = relatorConfig.getType();
            if (relatorName != null && relatorType != null) {
                Relator relator = newRelator(relatorConfig);
                relators.put(relatorName, relator);
            }
        }
    }

    private Relator newRelator(RelatorConfiguration relatorConfig) throws ClassNotFoundException {
        String relatorType = relatorConfig.getType();
        String loaderName = relatorConfig.getLoader();

        Relator relator;
        Class relatorClass = Class.forName(this.getClass().getPackage().getName() + "." + relatorType);
        Loader loader = loaders.get(loaderName);
        if (loader == null) throw new ClassNotFoundException("Loader " + loaderName + " is not defined");
        if (Relator.class.isAssignableFrom(relatorClass)) {
            try {
                Constructor<Relator> c = relatorClass.getConstructor(Loader.class, RelatorConfiguration.class);
                relator = c.newInstance(loader, relatorConfig);
                return relator;
            } catch (InstantiationException | IllegalAccessException | SecurityException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException e) {
                throw new ClassNotFoundException(e.getMessage(), e);
            }
        } else throw new ClassNotFoundException(relatorType + " is not a valid subclass of Relator");
    }
}
